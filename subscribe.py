import app
from frappeclient import FrappeClient
import ConfigParser
import os

path = os.path.dirname(os.path.realpath(__file__))
config = ConfigParser.ConfigParser()
config.read(path + '/app.cfg')

db_file = config.get('General', 'db_file')
server_url = config.get('Server', 'url')
server_username = config.get('Server', 'username')
server_password = config.get('Server', 'password')


params = {"doctype": "Device",  "fields": "name, device_name, device_pin, room, status, type, tenant", "limit_page_length": "500"}
client = FrappeClient(server_url, server_username, server_password)
devices = client.get_api("frappe.client.get_list", params)
for dev in devices:
    pin = int(dev['device_pin']) - 1
    device, created = app.Device.get_or_create(name=dev['device_name'], pin=pin, view_pin=int(dev['device_pin']))

    if dev['tenant']:
        device.tenant = dev['tenant']

    if dev['room']:
        device.room = dev['room']

    device.status = str(dev['status']).strip()
    device.save()

