from gcloud import pubsub

import ConfigParser
import os
import json
import app
import datetime

path = os.path.dirname(os.path.realpath(__file__))

config = ConfigParser.ConfigParser()
config.read(path + '/app.cfg')


client = pubsub.client.Client.from_service_account_json(config.get('Messaging Server', 'credentials_path',
                                                                   path + '/erpnext.json'),
                                                        config.get('Messaging Server', 'project_id', ''))

topic = client.topic("pad_user")
subscription = topic.subscription("pad_user")
messages = subscription.pull(max_messages=50)

for ackid, message in messages:
    data = json.loads(message.data)
    print message.data
    user, created = app.User.get_or_create(user_name=data['user_name'])
    user.password = data['password']
    user.status = data['status']
    user.first_name = data['first_name']
    user.last_name = data['last_name']
    user.save()
    subscription.acknowledge(ackid)


