import app
from crontab import CronTab
import sys
import os

path = os.path.dirname(os.path.realpath(__file__))

command = 'cd ' + path + ' && ' + sys.executable + ' subscribe.py > /media/data/logs/subscribe.log 2>&1'
cron = CronTab(user=True)
cron.remove_all(comment='subscribe messaging server')
job = cron.new(command=command, comment='subscribe messaging server')
job.minute.every(4)
job.enable()
cron.write_to_user(user=True)

command = 'cd ' + path + ' && ' + sys.executable + ' user_update.py > /media/data/logs/user_update.log 2>&1'
cron = CronTab(user=True)
cron.remove_all(comment='update user data')
job = cron.new(command=command, comment='update user data')
job.minute.every(2)
job.enable()
cron.write_to_user(user=True)

command = 'cd ' + path + ' && ' + sys.executable + ' publish.py > /media/data/logs/publish.log 2>&1'
cron = CronTab(user=True)
cron.remove_all(comment='publish messaging server')
job = cron.new(command=command, comment='publish messaging server')
job.minute.every(8)
job.enable()
cron.write_to_user(user=True)

command = 'cd ' + path + ' && ' + sys.executable + ' notify.py > /media/data/logs/notify.log 2>&1'
cron = CronTab(user=True)
cron.remove_all(comment='notify offline')
job = cron.new(command=command, comment='notify offline')
job.minute.every(40)
job.enable()
cron.write_to_user(user=True)


app.create_and_connect()
