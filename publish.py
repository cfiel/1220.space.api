import app
from frappeclient import FrappeClient
import ConfigParser
import os
import json
import time


path = os.path.dirname(os.path.realpath(__file__))
config = ConfigParser.ConfigParser()
config.read(path + '/app.cfg')

db_file = config.get('General', 'db_file')
server_url = config.get('Server', 'url')
server_username = config.get('Server', 'username')
server_password = config.get('Server', 'password')

client = FrappeClient(server_url, server_username, server_password)
count = 0
while count <= 300:
    messages = app.Message.select().where(app.Message.sent == False).limit(1)
    for message in messages:
        date_id = str(message.created_at.year) + str(message.created_at.month) + str(message.created_at.day)
        data = json.loads(str(message.data).replace("'", "\""))
        data['id'] = date_id + str(message.id)
        params = {"message": json.dumps(data)}
        res = client.post_api("metromatik.rest.process_messages", params)
        if res['status'] == 'ok':
            message.sent = True
            message.save()
        time.sleep(1)
    count += 1

app.set_offline_device()
app.delete_long_sessions()
