#!/usr/bin/env bash
apt-get update
apt-get -y install git
apt-get -y install gunicorn
apt-get -y install python-pip python-dev build-essential
apt-get -y install sqlite3 libsqlite3-dev
apt-get -y install monit
mkdir /media/data
echo "/dev/sda1 /media/data   ext4  defaults       0  0" > /etc/fstab
mount -a
git clone https://cfiel@bitbucket.org/cfiel/1220.space.api.git
cd 1220.space.api
pip install -r requirements.txt
./init.sh
update-rc.d flask defaults
cp interfaces /etc/network
/etc/init.d/networking restart
/etc/init.d/flask start
echo "check process flask with pidfile /root/1220.space.api/gunicorn_pid" > /etc/monit/monitrc
echo "start program = \"/etc/init.d/flask start\"" > /etc/monit/monitrc
echo "stop program = \"/etc/init.d/flask stop\"" > /etc/monit/monitrc

