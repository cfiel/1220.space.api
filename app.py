from flask_admin.contrib.peewee import ModelView
from flask_admin import Admin
from peewee import *
from playhouse.sqlite_ext import SqliteExtDatabase
import datetime
import json
import ConfigParser
import os
from datetime import timedelta
from flask import request, Response, Flask, render_template, redirect
from werkzeug.exceptions import HTTPException
from frappeclient import FrappeClient
import urllib2


path = os.path.dirname(os.path.realpath(__file__))

config = ConfigParser.ConfigParser()
config.read(path + '/app.cfg')

db_file = config.get('General', 'db_file')
server_url = config.get('Server', 'url')
db = SqliteExtDatabase(str(db_file), threadlocals=False)
app = Flask(__name__)
app.config['ADMIN_CREDENTIALS'] = (config.get('General', 'username'), config.get('General', 'password'))
admin = Admin(app, name='API Server', template_mode='bootstrap3')


class BaseModel(Model):
    class Meta:
        database = db


class Device(BaseModel):
    name = CharField()
    pin = IntegerField()
    view_pin = IntegerField()
    status = CharField(default='On')
    kind = CharField(default='')
    description = CharField(null=True)
    tenant = CharField(null=True)
    room = CharField(null=True)
    switch_update_at = DateTimeField(default=datetime.datetime.now)
    sensor_update_at = DateTimeField(default=datetime.datetime.now)
    state = CharField(default='Online')

    class Meta:
        order_by = ('name', 'pin')


class Message(BaseModel):
    data = TextField()
    from_type = CharField()
    sent = BooleanField(default=False)
    created_at = DateTimeField(default=datetime.datetime.now)


class User(BaseModel):
    user_name = TextField()
    password = TextField(null=True)
    status = BooleanField(default=True)
    first_name = TextField(null=True)
    last_name = TextField(null=True)


class Session(BaseModel):
    token = TextField(null=True)
    created_at = DateTimeField(default=datetime.datetime.now)
    user = ForeignKeyField(User, related_name='sessions')
    mac = TextField(null=True)
    ip = TextField(null=True)


class DeviceModelView(ModelView):
    can_delete = False  # disable model deletion
    can_edit = False
    can_create = False
    page_size = 100  # the number of entries to display on the list view
    column_editable_list = ['status']
    column_exclude_list = ['description', 'switch_update_at', 'sensor_update_at']
    column_list = ('name', 'view_pin', 'kind', 'status', 'state', 'room', 'tenant')
    column_labels = dict(view_pin='PIN')
    column_filters = ['name', 'room', 'tenant']

    def get_query(self):
        return self.model.select()

    def is_accessible(self):
        auth = request.authorization or request.environ.get('REMOTE_USER')  # workaround for Apache
        if not auth or (auth.username, auth.password) != app.config['ADMIN_CREDENTIALS']:
            raise HTTPException('', Response(
                "Please log in.", 401,
                {'WWW-Authenticate': 'Basic realm="Login Required"'}
            ))
        return True


class MessageModelView(ModelView):
    can_delete = False  # disable model deletion
    can_edit = False
    can_create = False
    page_size = 50  # the number of entries to display on the list view
    column_list = ('data', 'sent', 'created_at')
    column_labels = dict(created_at='Created at')
    column_filters = ['sent', 'created_at']
    column_default_sort = ('created_at', True)

    def is_accessible(self):
        auth = request.authorization or request.environ.get('REMOTE_USER')  # workaround for Apache
        if not auth or (auth.username, auth.password) != app.config['ADMIN_CREDENTIALS']:
            raise HTTPException('', Response(
                "Please log in.", 401,
                {'WWW-Authenticate': 'Basic realm="Login Required"'}
            ))
        return True


def create_and_connect():
    db.connect()
    db.create_tables([Device, Message, User, Session], safe=True)


def delete_long_sessions():
    long_session = Session.delete().where(Session.created_at < datetime.datetime.now() - timedelta(hours=1))
    long_session.execute()


@app.route('/wifidog/ping/')
def pong():
    return 'pong'


@app.route('/wifidog/portal/')
def portal():
    return 'success!!!'


@app.route('/wifidog/auth/')
def auth():
    # stage = request.args.get('stage', '')
    # incoming = request.args.get('incoming', '')
    # outgoing = request.args.get('outgoing', '')

    ip = request.args.get('ip', '')
    mac = request.args.get('mac', '')
    token = request.args.get('token', '')

    dup = Session.delete().where(Session.mac == mac, Session.token != token)
    dup.execute()

    try:
        session = Session.get(token=token)
    except Session.DoesNotExist:
        return 'Auth: 0'

    try:
        user = User.get(id=session.user)
    except User.DoesNotExist:
        return 'Auth: 0'
    session.ip = ip
    session.mac = mac
    session.save()

    clean_session(user)

    return 'Auth: 1'


def clean_session(user):
    sessions = user.sessions.order_by(-Session.created_at)
    if user.status:
        count = 0
    else:
        count = 3
    for session in sessions:
        count = count + 1
        if count >= 3:
            Session.delete().where(Session.token==session.token).execute()


@app.route('/wifidog/gw_message/')
def gw_message():
    message = request.args.get('message', '')
    return message


@app.route('/wifidog/login/', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        gw_address = request.form['gw_address']
        gw_port = request.form['gw_port']

        try:
            urllib2.urlopen(server_url, timeout=1).getcode()
            client = FrappeClient(server_url, username, request.form['password'])
            user, created = User.get_or_create(user_name=username, password=request.form['password'])
            if ("csharpinc.com" in username) or ("Administrator" in username):
                status = 'On'
            else:
                tenant = client.get_api("metromatik.rest.tenant_status")
                status = tenant['status'][0][0]
                user.password = request.form['password']
                user.status = status
                user.save()
        except:
            user = None
            status = 'Off'
            q_user = User.get(user_name=username)
            if q_user:
                if q_user.password == request.form['password']:
                    user = q_user
                    status = user.status

        if user:
            if status == 'Off':
                clean_session(user)
                return render_template('login.html', gw_address=gw_address, gw_port=gw_port,
                                       message="Deactivated. Please reload account")
            else:
                import uuid
                token = uuid.uuid4().hex
                session = Session.create(user=user, token=token)
                session.save()
                return redirect('http://' + gw_address + ':' + gw_port + '/wifidog/auth?token=' + token, 301)
        else:
            return render_template('login.html', gw_address=gw_address, gw_port=gw_port,
                                   message="Invalid User or password")

    else:
        gw_address = request.args.get('gw_address', '')
        gw_port = request.args.get('gw_port', '')
        return render_template('login.html', gw_address=gw_address, gw_port=gw_port)


@app.route('/api/d', methods=['GET'])
def device_route():
    if request.method == 'GET':
        name = str(request.args.get('n', ''))
        data = request.args.get('d', '')
        kind = str(request.args.get('k', ''))
        arr_data = data.split('a')
        count = 0
        val_dict = ""
        for val in arr_data:
            if val:
                val_dict += str(count) + " = " + str(val) + ","
                device, created = Device.get_or_create(name=name, pin=count, view_pin=count + 1)
                device.sensor_update_at = datetime.datetime.now()
                device.kind = kind
                device.state = 'Online'
                device.save()
                count += 1
        created_at = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        message = {'data': val_dict, 'name': name, 'kind': kind, 'created_at': created_at}
        new = Message(data=message, sent=False, from_type='sensor_read')
        new.save()

        status = {}
        for device in Device.select().where(Device.name == name):
            status[str(device.pin)] = str(device.status).upper()
            device.switch_update_at = datetime.datetime.now()
            device.state = 'Online'
            device.save()
        print status
        return json.dumps(status)


@app.route('/api/sensor', methods=['GET', 'POST'])
def sensor():
    if request.method == 'POST':
        req_data = json.loads(str(request.get_data()))
        data = req_data['data']
        name = req_data['name']
        kind = req_data['kind']
        created_at = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        message = {'data': data, 'name': name, 'kind': kind, 'created_at': created_at}
        new = Message(data=json.dumps(message), sent=False, from_type='sensor_read')
        new.save()
        values = data.split(",")
        for value in values:
            make = value.split("=")
            if make[0]:
                device, created = Device.get_or_create(name=name, pin=make[0], view_pin=int(make[0]) + 1)
                device.sensor_update_at = datetime.datetime.now()
                device.kind = kind
                device.state = 'Online'
                device.save()

    if request.method == 'GET':
        name = str(request.args.get('n', ''))
        data = request.args.get('d', '')
        kind = str(request.args.get('k', ''))
        arr_data = data.split('a')
        count = 0
        val_dict = ""
        for val in arr_data:
            if val:
                val_dict += str(count) + " = " + str(val) + ","
                device, created = Device.get_or_create(name=name, pin=count, view_pin=count + 1)
                device.sensor_update_at = datetime.datetime.now()
                device.kind = kind
                device.state = 'Online'
                device.save()
                count += 1
        created_at = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        message = {'data': val_dict, 'name': name, 'kind': kind, 'created_at': created_at}
        new = Message(data=message, sent=False, from_type='sensor_read')
        new.save()

    return json.dumps({'status': 'OK'})


@app.route('/api/switch')
def switch():
    name = request.args.get('name', '')
    status = {}
    for device in Device.select().where(Device.name == name):
        print "ok"
        status[str(device.pin)] = str(device.status).upper()
        device.switch_update_at = datetime.datetime.now()
        device.state = 'Online'
        device.save()
    return json.dumps(status)


def set_offline_device():
        for device in Device.select():
            if device.switch_update_at + timedelta(hours=2) < datetime.datetime.now():
                device.state = 'Offline'
            else:
                device.state = 'Online'
            device.save()

admin.add_view(DeviceModelView(Device, 'Device'))
admin.add_view(MessageModelView(Message, 'Message'))

if __name__ == '__main__':
    create_and_connect()
    set_offline_device()
    app.run(host='0.0.0.0', port=8080, debug=True)


