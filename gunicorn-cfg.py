import multiprocessing

bind = "0.0.0.0:80"
workers = multiprocessing.cpu_count() * 2 + 1
pidfile='gunicorn_pid'
daemon = True
accesslog='gunicorn_access.log'
errorlog='gunicorn_error.log'
loglevel = 'debug'
