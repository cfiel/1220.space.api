import ConfigParser
import os
import json
import app

path = os.path.dirname(os.path.realpath(__file__))

config = ConfigParser.ConfigParser()
config.read(path + '/app.cfg')
tech_number = config.get('General', 'technical_mobile_numbers')


def send_sms(name, mobile_number, message):
    import httplib
    data = json.dumps({'user_email': 'ccfiel@gmail.com', 'user_token': 'fthvY8iDzUf5yQABQcgv',
                       'name': name, 'mobile_number': mobile_number,
                       'message': message, 'pay': True})
    h = httplib.HTTPConnection('smsblastr.com:80')
    headers = {"Content-type": "application/json", "Accept": "application/json"}
    h.request('POST', '/api/v1/send_number', data, headers)
    r = h.getresponse()

for device in app.Device.select():
    if device.state == 'Offline':
        message = device.room + " is offline. Restart router please."
        for number in str(tech_number).split(","):
            send_sms("metromatik", number, message)
        break

